//
//  SearchRequestManager.swift
//  SsaBusiness
//
//  Created by hu on 09/12/2021.
//

import UIKit
import SsaCommon

class SsaSearchRequestManager {
    init(){}
    let manager : SsaNetworking = SsaNetworking.ShareInstance
    
}

extension SsaSearchRequestManager{
    func getComingFilmsData(block:@escaping (Any?,Any?)->Void) {
        manager.getDataRequest(path: "/ptapi/getComingFilms?ci=80&limit=10") { response in
            block(response,nil)
        } failure: { error in
            block(nil,error)
        }
    }
}
