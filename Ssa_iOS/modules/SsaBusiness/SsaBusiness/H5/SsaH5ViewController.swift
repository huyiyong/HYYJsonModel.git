//
//  SsaH5ViewController.swift
//  SsaBusiness
//
//  Created by hu on 10/12/2021.
//

import UIKit
import SsaCommon
import WebKit

open class SsaH5ViewController: BaseViewController {
     
    lazy var webView : WKWebView = {
        let preferences = WKPreferences()
        let configuration : WKWebViewConfiguration = WKWebViewConfiguration.init()
        configuration.preferences = preferences
        let userContentVC : WKUserContentController = WKUserContentController.init()
        configuration.userContentController = userContentVC
        
        let web : WKWebView = WKWebView(frame: CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height), configuration: configuration)
        
        return web
    }()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        self.view.addSubview(webView)
        
        let urlStr : String = "https://www.baidu.com"
        let url : URL = URL(string: urlStr)!
        let request : URLRequest = URLRequest(url: url)
        webView.load(request)
    }
    
    
}
