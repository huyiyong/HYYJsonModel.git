//
//  SsaUniAppServer.h
//  Ssa_iOS
//
//  Created by hu on 10/12/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SsaUniAppServer : NSObject

+ (instancetype)shareInstance;

- (void)registerSDKEngineWithLaunchOptions:(NSDictionary*)options;

/// 通过指定的全路径页面路径打开uniapp页面
/// @param path 路径页面
- (void)uniAppActivityWithRedirectPath:(NSString *)path;

@end

NS_ASSUME_NONNULL_END
