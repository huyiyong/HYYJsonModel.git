//
//  SsaUniAppServer.m
//  Ssa_iOS
//
//  Created by hu on 10/12/2021.
//

#import "SsaUniAppServer.h"
#import <DCUniMPConfiguration.h>
#import <DCUniMPSDKEngine.h>
#import <WXSDKEngine.h>
#import <ZipZap/ZipZap.h>

#define ActivityAppId @"__UNI__8B69DE0"

@interface SsaUniAppServer ()
@property (nonatomic, weak) DCUniMPInstance *uniMPInstance;

@end

@implementation SsaUniAppServer

+ (instancetype)shareInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

/// 服务注册
/// @param options 启动项
- (void)registerSDKEngineWithLaunchOptions:(NSDictionary*)options {
    NSMutableDictionary *optionsDict = [NSMutableDictionary dictionaryWithDictionary:nil];
    [optionsDict setObject:[NSNumber numberWithBool:YES] forKey:@"debug"];
    //     初始化引擎
    [DCUniMPSDKEngine initSDKEnvironmentWithLaunchOptions:optionsDict];
    //    该字符串"JyUniApp" 是与uniapp那边约定的字段，但是本地文件一定也要以这个命名，详见宏 WX_EXPORT_METHOD_SYNC
    [WXSDKEngine registerModule:@"SsaUniApp" withClass:NSClassFromString(@"SsaUniApp")];
    [self prepareResource];
}

// 资源包准备
- (void)prepareResource {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//        BHEnvironmentType env = [BHContext shareInstance].env;
//        if (env == BHEnvironmentProd) {
        if (![self checkUniMPResource]) {
            NSLog(@"资源包环境有问题,冷更新执行失败!!!");
            return;
        }else {
            NSLog(@"资源包已部署本地环境");
        }
        //        }else {
        //            // 如果是开发环境 则每次都强制覆盖展开
        //            [self releaseAppResourceWithAppid:ActivityAppId];
        //        }
    });
}


- (void)preloadUniAppWithRedirectPath:(NSString *)path {
    if (![self checkUniMPResource]) {
        NSLog(@"资源包环境有问题,业务不可向下执行!!!");
        return;
    }
    [self setUniMPMenuItems];
    
    DCUniMPConfiguration *configuration = [[DCUniMPConfiguration alloc] init];
    configuration.enableBackground = NO;
    configuration.redirectPath = path;
    [DCUniMPSDKEngine preloadUniMP:ActivityAppId configuration:configuration completed:^(DCUniMPInstance * _Nullable uniMPInstance, NSError * _Nullable error) {
        if (uniMPInstance) {
            // 预加载后打开小程序
            [uniMPInstance showWithCompletion:^(BOOL success, NSError * _Nullable error) {
                
                if (error) {
                    NSLog(@"show 小程序失败：%@",error);
                }
            }];
        } else {
            NSLog(@"预加载小程序出错：%@",error);
        }
    }];
}

// MARK: - DCUniMPSDKEngineDelegate
- (void)defaultMenuItemClicked:(NSString *)identifier{
    NSLog(@"标识为 %@ 的 item 被点击了", identifier);
    __weak typeof(self) weakSelf = self;
    if ([identifier isEqualToString:@"goCart"]) {
        // 到购物车
        [self.uniMPInstance closeWithCompletion:^(BOOL success, NSError * _Nullable error) {
           
            if (success) {
                NSLog(@"小程序 %@ 进入后台",weakSelf.uniMPInstance.appid);
            } else {
                NSLog(@"hide 小程序出错：%@",error);
            }
            
            
        }];
    }else if ([identifier isEqualToString:@"goHome"]) {
        // 到首页
      
        [self.uniMPInstance closeWithCompletion:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                NSLog(@"小程序 closed");
            } else {
                NSLog(@"close 小程序出错：%@",error);
            }
          
           
        }];
    }
}

- (void)onUniMPEventReceive:(NSString *)event
                       data:(id)data
                   callback:(DCUniMPKeepAliveCallback)callback {
    NSLog(@"Receive UniMP event: %@ data: %@",event,data);
    // 回传数据给小程序
    // DCUniMPKeepAliveCallback 用法请查看定义说明
    if (callback) {
        callback(@"native callback message",NO);
    }
}

- (void)uniMPOnClose:(NSString *)appid {
    NSLog(@"小程序 %@ 被关闭了",appid);
    self.uniMPInstance = nil;
    
}

/// 通过指定的全路径页面路径打开uniapp页面
/// @param path 路径页面
- (void)uniAppActivityWithRedirectPath:(NSString *)path {
    if (![self checkUniMPResource]) {
        NSLog(@"资源包环境有问题,业务不可向下执行!!!");
        return;
    }
    [self setUniMPMenuItems];
    
    DCUniMPConfiguration *configuration = [[DCUniMPConfiguration alloc] init];
    configuration.enableBackground = NO;
    configuration.redirectPath = path;
    configuration.openMode = DCUniMPOpenModePush;
//    @weakify(self)
    
    [DCUniMPSDKEngine openUniMP:@"__UNI__8B69DE0" configuration:configuration completed:^(DCUniMPInstance * _Nullable uniMPInstance, NSError * _Nullable error) {
        NSLog(@"ssdddsdd-----:%@ error===:%@",uniMPInstance,error);
//        if (uniMPInstance) {
//            @strongify(self);
//            self.uniMPInstance = uniMPInstance;
//        } else {
//            TLogError(@"打开小程序出错：%@",error);
//        }
    }];
}

// MARK: - Private
/// 检测预置环境
- (BOOL)checkUniMPResource {
    if (![DCUniMPSDKEngine isExistsApp:ActivityAppId]) {
        return [self releaseAppResourceWithAppid:ActivityAppId];
    }else {
        // 如果存在则对比版本
        NSDictionary *manifest = [self getManifest];
        if (!manifest ) {
            return [self releaseAppResourceWithAppid:ActivityAppId];
        }
        NSDictionary *versionItem = manifest[@"version"];
        // 固化包版本
        NSInteger bundleCode = [versionItem[@"code"] integerValue];
        
        // 本地沙盒版本
        NSDictionary *localVersion = [DCUniMPSDKEngine getUniMPVersionInfoWithAppid:ActivityAppId];
        NSInteger localCode = [localVersion[@"code"] integerValue];
        
        if (localCode < bundleCode) {
            return [self releaseAppResourceWithAppid:ActivityAppId];
        }
    }
    return YES;
}

/// 重载资源包
/// @param appId appid
- (BOOL)releaseAppResourceWithAppid:(NSString *)appId {
    // 读取导入到工程中的wgt应用资源
    NSString *path = [[NSBundle mainBundle] pathForResource:appId ofType:@"wgt"];
    if (!path) {
        NSLog(@"资源路径不正确，请检查");
        return NO;
    }
    return [DCUniMPSDKEngine releaseAppResourceToRunPathWithAppid:appId
                                                 resourceFilePath:path];
}

/// 从bundle中的wgt包中直接读取manifest配置文件
- (NSDictionary *)getManifest {
    NSString *wgtPath = [[NSBundle mainBundle] pathForResource:ActivityAppId ofType:@"wgt"];
    
    NSError *error = nil;
    ZZArchive *archive = [ZZArchive archiveWithURL:[NSURL fileURLWithPath:wgtPath]
                                                error:&error];
    if (error) {
        return nil;
    }
    NSDictionary *manifestDic = nil;
    for (ZZArchiveEntry *entry in archive.entries) {
        if ([entry.fileName isEqualToString:@"manifest.json"]) {
            NSData *jsonData = [entry newDataWithError:&error];
            if (!error) {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:entry.encoding];
                NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                manifestDic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&error];
            }
        }
    }
    return manifestDic;
}

///    配置胶囊按钮菜单
/// ActionSheet 全局项（点击胶囊按钮 ··· ActionSheet弹窗中的项）
- (void)setUniMPMenuItems {
    DCUniMPMenuActionSheetItem *item1 = [[DCUniMPMenuActionSheetItem alloc] initWithTitle:@"购物车" identifier:@"goCart"];
    DCUniMPMenuActionSheetItem *item2 = [[DCUniMPMenuActionSheetItem alloc] initWithTitle:@"返回首页" identifier:@"goHome"];
    [DCUniMPSDKEngine setDefaultMenuItems:@[item1,item2]];
    
    DCUniMPMenuActionSheetStyle *actionSheetStyle = [DCUniMPMenuActionSheetStyle new];
    actionSheetStyle.textColor = @"#210201";
    actionSheetStyle.fontSize = 14;
    actionSheetStyle.fontWeight = @"normal";
    [DCUniMPSDKEngine configMenuActionSheetStyle:actionSheetStyle];
    [DCUniMPSDKEngine setDelegate:self];
}


@end
