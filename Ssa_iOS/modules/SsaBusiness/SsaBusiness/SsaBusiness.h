//
//  SsaBusiness.h
//  SsaBusiness
//
//  Created by hu on 02/12/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for SsaBusiness.
FOUNDATION_EXPORT double SsaBusinessVersionNumber;

//! Project version string for SsaBusiness.
FOUNDATION_EXPORT const unsigned char SsaBusinessVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SsaBusiness/PublicHeader.h>

#import "SsaUniAppServer.h"
#import "SsaUniApp.h"

