//
//  SsaHomeVC.swift
//  SsaBusiness
//
//  Created by hu on 02/12/2021.
//

import UIKit
import SsaCommon

open class SsaHomeVC: BaseViewController {

    open override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let ss : SsaUniAppServer = SsaUniAppServer()
        ss.uniAppActivity(withRedirectPath: "pages/integral/home?action=redirect")
    }


}
