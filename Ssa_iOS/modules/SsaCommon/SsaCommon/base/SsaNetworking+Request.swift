//
//  SsaNetworking+Request.swift
//  SsaCommon
//
//  Created by hu on 09/12/2021.
//

import UIKit

extension SsaNetworking {
    ///get请求demo
   public func getDataRequest(path: String,
                            success: @escaping SsaResponseSuccess,
                            failure: @escaping SsaResponseFail) {
        
        SsaNetworking.ShareInstance.getWith(path: path, params: nil, success: { (response) in
            guard let json = response as? [String: Any] else { return }
#if DEBUG
            print("resultJson格式======url:\(path)=======:\(json)")
#endif
            ///保证接口调通, 否则返回错误信息
            guard json["status"] as? NSNumber == 1 else {
//                MBProgressHud.showTextHudTips(message: json["msg"] as? String)
                print(json["msg"] as? String ?? "")
                failure(response)
                return
            }
            guard let dict = json["obj"] as? [String: Any] else {
                failure(NSError(domain: "转字典失败", code: 2000, userInfo: nil))
                return
            }
            guard let dataArray = dict["data"] else {
                failure(NSError(domain: "获取数组失败", code: 2000, userInfo: nil))
                return
            }
            success(dataArray as AnyObject)
        },error: { (error) in
            failure(error)
//            MBProgressHud.showTextHudTips(message: "网络请求错误")
        })
    }
    ///post请求demo
    public func postDataRequest(path: String,
                             param:[String:Any]?,
                             success: @escaping SsaResponseSuccess,
                             failure: @escaping SsaResponseFail) {
//        let path = "v1/passport/register"
        SsaNetworking.ShareInstance.postWith(path: path, params: param, success: { (response) in
            guard let json = response as? [String: Any] else { return }
            
#if DEBUG
            print("resultJson格式======url:\(path)=======:\(json)")
#endif
            
            guard json["status"] as? NSNumber == 1 else {
                //                MBProgressHud.showTextHudTips(message: json["msg"] as? String)
                print(json["msg"] as? String ?? "")
                failure(response)
                return
            }
            success(response as AnyObject)
        },error: { (error) in
            failure(error)
            //            MBProgressHud.showTextHudTips(message: "网络请求错误")
        })
    }
}
