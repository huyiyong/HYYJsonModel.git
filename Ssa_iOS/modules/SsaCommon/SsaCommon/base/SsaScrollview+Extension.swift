//
//  TableView+Extension.swift
//  SsaCommon
//
//  Created by hu on 09/12/2021.
//

import UIKit

extension UITableView{
   
  public  func registerCell<T:UITableViewCell>(cell: T.Type){
        self.register(cell.self, forCellReuseIdentifier: String(cell.description()))
    }
    
  public  func dequeueCell<T:UITableViewCell>(cell:T.Type,index:IndexPath)->UITableViewCell{
        
        return self.dequeueReusableCell(withIdentifier: String(cell.description()), for: index)
    }
}


extension UICollectionView{
   public func registerCell<T:UICollectionViewCell>(cell: T.Type){
        self.register(cell.self, forCellWithReuseIdentifier: String(cell.description()))
    }
    
   public func dequeueCell<T:UICollectionViewCell>(cell:T.Type,index:IndexPath)->UICollectionViewCell{
        
        return self.dequeueReusableCell(withReuseIdentifier: String(cell.description()), for: index)
        
    }
}

