//
//  SsaConstant.swift
//  SsaCommon
//
//  Created by hu on 09/12/2021.
//

import UIKit


public let Screen = UIScreen.main.bounds.size

public let isIphoneX : Bool = {
    var isIphonex : Bool = false
    if #available(iOS 11.0, *){
        isIphonex = (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0) > 0.1 ? true : false
    }
    return isIphonex
}()

public let iphoneAreaInsets : UIEdgeInsets = {
    var iphoneAreaInsets : UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    if #available(iOS 11.0, *){
        iphoneAreaInsets = UIApplication.shared.keyWindow!.safeAreaInsets
    }

    return iphoneAreaInsets
}()

struct SsaConstant{
    
}
