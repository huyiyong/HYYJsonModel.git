//
//  Dictionray+Category.swift
//  SsaCommon
//
//  Created by hu on 08/12/2021.
//

import UIKit

public extension Dictionary{
    
    func transFromDictionaryToJsonString()->String{
        if(!JSONSerialization.isValidJSONObject(self)){
            return ""
        }
        let data : NSData! = try? JSONSerialization.data(withJSONObject: self, options: []) as NSData?
        let jsonString = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue)
        return jsonString! as String
    }
}
