//
//  String+md5.swift
//  SsaCommon
//
//  Created by hu on 08/12/2021.
//

import UIKit
import CommonCrypto

public extension String {
    
    var length: Int {
        ///更改成其他的影响含有emoji协议的签名
        return self.utf16.count
    }
    
    ///是否包含字符串
    func containsIgnoringCase(find: String) -> Bool {
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    /**
     - returns: the String, as an MD5 hash.
     */
    var md5Str: String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        CC_MD5(str!, strLen, result)

        let hash = NSMutableString()

        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }

        result.deallocate()
        return hash as String
    }
    
    func md5() -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)

        CC_MD5(str!, strLen, result)
        let hash = NSMutableString()
        for num in 0 ..< digestLen {
            hash.appendFormat("%02x", result[num])
        }
        result.deinitialize(count: 1)

        return String(format: hash as String)
    }
    
    /// 截取任意位置到结束
    ///
    /// - Parameter end:
    /// - Returns: 截取后的字符串
    func stringCutToEnd(star: Int) -> String {
        if !(star < count) { return "截取超出范围" }
        let sRang = index(startIndex, offsetBy: star)..<endIndex
        return String(self[sRang])
//        return substring(with: sRang)
    }
    
    
    var getUUID : String{
       var string = ""
        for _ in 0...32{
           let  number = arc4random() % 36
            if(number < 10){
              let figure = arc4random() % 10
                let tempStr = String(figure)
                string += tempStr
            }else{
                let figure = (arc4random() % 26) + 97
                let charcter = Character(UnicodeScalar(figure)!)
                let tempStr = String(charcter)
                string += tempStr
            }
        }
        
        return  string
    }
    
    func cutKey(content: inout String) -> String{
        
        if(!content.isEmpty && content.count < 27){
            content = content + "000000000000000000000000000"
        }
        
        return content
    }
    
    func getOdd(content:inout String)->String{
        
        
        var str : String = ""
        
        var tempIndex = 0
        for childStr in content{
            if(tempIndex%2 == 0){
                str.append(childStr)
            }
            
            tempIndex = tempIndex + 1
        }
        
       
        return str
    }
    
    
    func subString(str:String ,index: Int,count:Int) -> String{
        
        var tempStr = "";
        var tempIndex = 0;
        for childStr   in str {
            if(tempIndex>=index && tempIndex<=index+count){
                tempStr.append(childStr)
            }
            tempIndex = tempIndex + 1
        }
        return tempStr
    }
    
    
     func getConvert(apiStr: String, content:inout String,requestTimeStr:String) -> String{
         
         content =  content.data(using: .utf8)?.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0)) ?? ""
        
         var tempApiStr = apiStr
         
         var key = "".getUUID
         
         var nonceStr = "".getUUID
         
         let skey = content.cutKey(content: &content) + apiStr.getOdd(content: &tempApiStr) + requestTimeStr
         
         let validateSign = (content + skey).md5Str
         
         let tempOdd = key.getOdd(content: &key)
         let oddStr = tempOdd.subString(str: tempOdd, index: 0, count: 5)
         
         let tempNonce = nonceStr.getOdd(content: &nonceStr)
         let nonceString = tempNonce.subString(str: tempNonce, index: 0, count: 6)
         
         
         
        return ""
    }
    
}




