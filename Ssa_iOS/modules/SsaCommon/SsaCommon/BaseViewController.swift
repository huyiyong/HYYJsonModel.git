//
//  BaseViewController.swift
//  SsaCommon
//
//  Created by hu on 02/12/2021.
//

import UIKit
import SnapKit

open class BaseViewController: UIViewController {
    
    open var headView : UIView = {
        let view  = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0));
        return view
    }()
    
    open var backBtn : UIButton = {
        let btn : UIButton = UIButton()
        let image : UIImage! = UIImage.init(named: "icon_back")
        btn.setImage(image, for: .normal)
        btn.addTarget(self, action: #selector(backBtnClick(btn:)), for: .touchUpInside)
        return btn
    }()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHeaderView();
    }
    
    func setupHeaderView(){
        self.view.addSubview(headView)
        headView.backgroundColor = .orange
        
        let barHeight = isIphoneX ? 84.0 : 64.0
        headView.frame = CGRect(x: 0, y: 0, width: Screen.width, height: barHeight)
        
        headView.addSubview(backBtn)
        backBtn.snp_makeConstraints { make in
            make.left.equalTo(5)
            make.bottom.equalTo(-5)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
    }
    
    @objc func backBtnClick(btn:UIButton){
        
    }

}
