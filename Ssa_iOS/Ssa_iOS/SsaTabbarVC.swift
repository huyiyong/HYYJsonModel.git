//
//  SsaTabbarVC.swift
//  Ssa_iOS
//
//  Created by hu on 02/12/2021.
//

import UIKit
import SsaCommon
import SsaBusiness

class SsaTabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.isTranslucent = false
        addChildVC();
    }
    
    func addChildVC(){
        
        
        let homeVC = SsaHomeVC()
        let categoryVC = SsaH5ViewController()
        let cartVC = SsaCartVC()
        let mineVC = SsaMineVC()
        
        let homeNavVC : UINavigationController = UINavigationController.init(rootViewController: homeVC)
        homeNavVC.navigationBar.isHidden = true
        
        let categoryNavVC = UINavigationController.init(rootViewController: categoryVC)
        categoryNavVC.navigationBar.isHidden = true
        
        let cartNavVC = UINavigationController.init(rootViewController: cartVC)
        cartNavVC.navigationBar.isHidden = true
        
        let mineNavVC = UINavigationController.init(rootViewController: mineVC)
        mineNavVC.navigationBar.isHidden = true
        
        let homeTabItem = UITabBarItem.init(title: "首页", image: UIImage.init(named: "pintuan_icon_home_selected03"), selectedImage: UIImage.init(named: "pintuan_icon_home_selected01"));
        homeTabItem.setTitleTextAttributes([.foregroundColor:UIColor.gray], for: .normal)
        homeTabItem.setTitleTextAttributes([.foregroundColor:UIColor.green], for: .selected)
        homeVC.tabBarItem = homeTabItem;
        
        let categoryTabItem = UITabBarItem.init(title: "分类", image: UIImage.init(named: "pintuan_icon_home_selected04"), selectedImage: UIImage.init(named: "pintuan_icon_home_selected02"));
        categoryTabItem.setTitleTextAttributes([.foregroundColor:UIColor.gray], for: .normal)
        categoryTabItem.setTitleTextAttributes([.foregroundColor:UIColor.green], for: .selected)
        categoryVC.tabBarItem = categoryTabItem;
        
        let cartTabItem = UITabBarItem.init(title: "购物车", image: UIImage.init(named: "pintuan_icon_home_selected04"), selectedImage: UIImage.init(named: "pintuan_icon_home_selected02"));
        cartTabItem.setTitleTextAttributes([.foregroundColor:UIColor.gray], for: .normal)
        cartTabItem.setTitleTextAttributes([.foregroundColor:UIColor.green], for: .selected)
        cartVC.tabBarItem = cartTabItem;
        
        let mineTabItem = UITabBarItem.init(title: "我的", image: UIImage.init(named: "pintuan_icon_home_selected04"), selectedImage: UIImage.init(named: "pintuan_icon_home_selected02"));
        mineTabItem.setTitleTextAttributes([.foregroundColor:UIColor.gray], for: .normal)
        mineTabItem.setTitleTextAttributes([.foregroundColor:UIColor.green], for: .selected)
        mineVC.tabBarItem = mineTabItem;
        
        self.viewControllers = [homeNavVC,categoryNavVC,cartNavVC,mineNavVC]
    }
    

}
