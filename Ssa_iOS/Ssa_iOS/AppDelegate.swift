//
//  AppDelegate.swift
//  Ssa_iOS
//
//  Created by hu on 02/12/2021.
//

import UIKit
import SsaBusiness



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .white
        let tabbarVC = SsaTabbarVC.init()
       
        self.window?.rootViewController = tabbarVC
        self.window?.makeKeyAndVisible()
        
        //如果把他声明在主工程  那么我如何在子工程里 调用这个属于主工程的sdk呢
        let uniApp : SsaUniAppServer = SsaUniAppServer.shareInstance()
        uniApp.registerSDKEngine(launchOptions: launchOptions ?? [UIApplication.LaunchOptionsKey.init(rawValue: ""): 0])
        
        return true
    }

}

